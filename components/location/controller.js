const store = require('./store');

// insert function
function addLocation(location, area, parent_id){
    return new Promise((resolve, reject) => {
        if( !location || !area){
            console.error('[LocationController] No hay nombre o localización');
            reject('Los datos son incorrectos');
            return false;
        }
        const fullLocation = {
            location: location,
            area: area,
            parent_id: parent_id,
            date: new Date(),
        };
        store.add(fullLocation);
        resolve(fullLocation);
    });
}

// list function
function getLocations(filterParent_id){
    return new Promise((resolve, reject) => {
        resolve(store.list(filterParent_id));
    })
}

// update function
function updateLocation(id, location, area, parent_id){
    return new Promise(async(resolve, reject) => {
        if (!id || !location || !area){
            reject('data inválida');
            return false;
        }
        if(!parent_id){
            const result = await store.updateText(id, location, area)
        }else{
            const result = await store.updateText(id, location, area, parent_id)
        }
        resolve(result);
    })
}

// delete function
function deleteLocation(id){
    return new Promise((resolve, reject) => {
        if(!id){
            reject('Id invalido');
            return false;
        }

        store.remove(id)
            .then(() => {
                resolve();
            })
            .catch(e => {
                reject(e);
            });
    });
}

module.exports = {
    addLocation,
    getLocations,
    updateLocation,
    deleteLocation,
};