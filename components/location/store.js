
const db = require('mongoose');
const Model = require('./model');

db.Promise = global.Promise;
const uri = 'mongodb://root:Secret123@cluster0-shard-00-02-a9wrf.mongodb.net,cluster0-shard-00-01-a9wrf.mongodb.net/BancoDeBogota?ssl=true&replicaSet=Main-shard-0&authSource=admin&retryWrites=true';

db.Promise = global.Promise;

db.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('[db] Conectada con éxito'))
  .catch(err => console.error('[db]', err));

function addLocation(location){
    const myLocation = new Model(location);
    myLocation.save();
}

async function getLocations(filterParent_id){
    let filter = {};
    if(filterParent_id!==null){
        filter = { parent_id: filterParent_id};
    }
    const locations = await Model.find(filter);
    return locations;
}

function removeLocation(id){
    return Model.deleteOne({
        _id:id
    });
}

async function updateText(id, location){
    const foundLocation = await Model.findOne({
        _id: id
    });
    foundLocation.location = location;
    const newLocation = await foundLocation.save();
    return newLocation;
}

module.exports = {
    add: addLocation,
    list: getLocations,
    updateText: updateText,
    remove: removeLocation,
} 