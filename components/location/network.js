const express = require('express');
const response = require('../../network/response');
const controller = require('./controller');
const  router = express.Router();

router.get('/', function(req, res){
    const filterLocations = req.query.parent_id || null;
    controller.getLocations(filterLocations)
    .then((locationList)=>{
        response.success(req, res, locationList, 200);
    })
    .catch(e => {
        response.error(req, res, 'Unexpected Error', 500, e)
    })
});

router.post('/', function(req, res){

    controller.addLocation(req.body.location, req.body.area, req.body.parent_id )
        .then((fullLocation)=>{
            response.success(req, res, fullLocation, 201);
        })
        .catch(e => {
            response.error(req, res, 'Informacion invalida', 400, 'Error en el controlador');
        });
});

router.patch('/:id', function(req, res){
    controller.updateLocation(req.params.id, req.body.location, req.body.area, req.body.parent_id)
        .then((data) => {
            response.success(req, res, data, 200);
        })
        .catch(e => {
         response.error(req, res, 'Error interno', 500, e)
        });
});

router.delete('/:id', function(req, res ){
    controller.deleteLocation(req.params.id)
        .then(()=>{
            response.success(req, res, `Localización ${req.params.id} eliminada`, 200);
        })
        .catch(e =>{
            response.error(req, res, 'Error interno', 500, e);
        })
});

module.exports = router;