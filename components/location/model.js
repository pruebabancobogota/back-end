const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const mySchema = new Schema({
    location: {
      type: String,
      required: true,  
    },
    area: {
      type: Number,
      required: true,  
    },
    parent_id:{
      type: String,
      required: false,
    },
    date: Date,
});

const model = mongoose.model('Location', mySchema);
module.exports = model;