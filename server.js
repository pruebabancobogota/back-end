const express = require('express');
const bodyParser = require('body-parser')
const router = require('./network/routes');
// const router = require('./components/location/network');


var app = express();
app.use(bodyParser.json());
router(app);

// app.use(router);

app.use('/app', express.static('public'));

app.listen(3002);
console.log('Iniciado en http://localhost:3002');
