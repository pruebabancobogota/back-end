# Installation in linux systems
 - Intall nodemon:
    run the following command in your terminal: 
    npm install -g nodemon
 - Install npm:
    run the following command in your terminal:
    npm install
 - run the server with nodemon with the following command
    nodemon serve
    note: you should be inside the project file

# End points
 - type:GET
    localhost:3002/location:
    list all records

 - type:POST
    localhost:3002/location:
    with example body:
    {
        "location":"Suba",
        "area":"12409.98",
        "parent_id":"127"
    }
    
    instert function

 - type:DELETE
       localhost:3002/location/:id
       delete a row or document

 - type:PATCH/PUT
       localhost:3002/location/:id
       whit body
       edit your row or document