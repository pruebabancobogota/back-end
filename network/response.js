exports.success = function (requ, res, location, status){
    res.status(status || 200).send({
        error:'',
        body: location
    });
}

exports.error = function (req, res, location, status, details){
    console.error('[Response Error]' + ' ' + details);
    res.status(status || 500).send({
        error: location,
        body: '',
    });
}