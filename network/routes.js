const express = require('express');
const location = require('../components/location/network')

const routes = function(server){
    server.use('/location', location);
}

module.exports = routes;